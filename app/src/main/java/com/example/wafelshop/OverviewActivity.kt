package com.example.wafelshop

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.wafelshop.data.CookieListAdapter
import com.example.wafelshop.domain.Product
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.overview_koeken.*

class OverviewActivity : AppCompatActivity() {
    private var adapter: CookieListAdapter? = null
    private var cookieList: ArrayList<Product>? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    lateinit var toolbar: ActionBar

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    fun onComposeAction(mi: MenuItem) {
        val saveAlert = AlertDialog.Builder(this)
        saveAlert.setTitle("Uitloggen")
        saveAlert.setMessage("Zeker dat je wilt uitloggen?")
        saveAlert.setPositiveButton("Ja") { dialogInterface: DialogInterface, i: Int ->
            var mAuth: FirebaseAuth? = FirebaseAuth.getInstance()
            mAuth!!.signOut()
            startActivity(Intent(this, MainActivity::class.java))
        }
        saveAlert.setNegativeButton("Nee") { dialogInterface: DialogInterface, i: Int ->
        }
        saveAlert.show()
    }

    override fun onRestart() {
        super.onRestart()
        startActivity(Intent(this, OverviewActivity::class.java));
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        var mAuth: FirebaseAuth? = FirebaseAuth.getInstance()
        if (mAuth!!.currentUser == null) {
            Toast.makeText(applicationContext, "U bent niet ingelogd", Toast.LENGTH_LONG).show()
            startActivity(Intent(this, MainActivity::class.java))
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.overview_koeken)
        toolbar = supportActionBar!!
        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigationView)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        cookieList = ArrayList<Product>()
        layoutManager = LinearLayoutManager(this)
        adapter = CookieListAdapter(cookieList!!, this)
        rv_myRecyclerView.layoutManager = layoutManager
        rv_myRecyclerView.adapter = adapter
        var cookieNameList: Array<String> = arrayOf("RainbowDash", "Chocolate Sprinkels", "Vanilla", "K K K Koekje")
        var cookiePrijsList: Array<Double> = arrayOf(1.5, 1.0, 1.0, 1.0)
        var cookiePicList: Array<Int> =
            arrayOf(R.drawable.cookie1, R.drawable.cookie2, R.drawable.cookie3, R.drawable.cookie1)
        for (i in 0..(cookieNameList.size - 1)) {
            var cookie = Product(0, "", 0.0, 0)
            cookie.kind = cookieNameList[i]
            cookie.price = cookiePrijsList[i]
            cookie.pic = cookiePicList[i]
            cookieList?.add(cookie)
        }
        adapter!!.notifyDataSetChanged()
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_overview -> {
                startActivity(Intent(this, OverviewActivity::class.java))
            }
            R.id.navigation_bestel -> {
                startActivity(Intent(this, BestelActivity::class.java))
            }
            R.id.navigation_add -> {
                startActivity(Intent(this, BestelOverviewActivity::class.java))
            }
        }
        false
    }
}