package com.example.wafelshop

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.wafelshop.data.OrderListAdapter
import com.example.wafelshop.domain.OrderDetails
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_bestel_overview.*
import java.util.HashMap

class BestelOverviewActivity : AppCompatActivity() {
    private var adapter: OrderListAdapter? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    lateinit var toolbar: ActionBar

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    fun onComposeAction(mi: MenuItem) {
        val saveAlert = AlertDialog.Builder(this)
        saveAlert.setTitle("Uitloggen")
        saveAlert.setMessage("Zeker dat je wilt uitloggen?")
        saveAlert.setPositiveButton("Ja") { dialogInterface: DialogInterface, i: Int ->
            var mAuth: FirebaseAuth? = FirebaseAuth.getInstance()
            mAuth!!.signOut()
            startActivity(Intent(this, MainActivity::class.java))
        }
        saveAlert.setNegativeButton("Nee") { dialogInterface: DialogInterface, i: Int ->
        }
        saveAlert.show()
    }

    override fun onRestart() {
        super.onRestart()
        startActivity(Intent(this, OverviewActivity::class.java));
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        var mAuth: FirebaseAuth? = FirebaseAuth.getInstance()
        if (mAuth!!.currentUser == null) {
            Toast.makeText(applicationContext, "U bent niet ingelogd", Toast.LENGTH_LONG).show()
            startActivity(Intent(this, MainActivity::class.java))
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bestel_overview)
        toolbar = supportActionBar!!
        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigationView)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        val db = FirebaseFirestore.getInstance()
        var allOrders = arrayListOf<OrderDetails>()

        db.collection("users")
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    for (document in task.result!!) {
                        if (mAuth!!.currentUser!!.email == "admin@hotmail.com") {
                            var currentNaam = document.data["first"] as String + " " + document.data["last"] as String
                            var currentOrders = document.data["orders"] as ArrayList<HashMap<String, String>>
                            for (i in 0 until currentOrders.size) {
                                var orderDetail = OrderDetails(null, null, null, null, null, null)
                                var long1 = currentOrders[i]["amount1"] as Long
                                var long2 = currentOrders[i]["amount2"] as Long
                                var long3 = currentOrders[i]["amount3"] as Long
                                var long4 = currentOrders[i]["amount3"] as Long

                                orderDetail.adres = currentOrders[i]["adres"]
                                orderDetail.amount1 = long1.toInt()
                                orderDetail.naam = currentNaam
                                orderDetail.amount2 = long2.toInt()
                                orderDetail.amount3 = long3.toInt()
                                orderDetail.amount4 = long4.toInt()
                                allOrders.add(orderDetail)
                            }
                            layoutManager = LinearLayoutManager(this)
                            adapter = OrderListAdapter(allOrders!!, this)
                            rv_myRecyclerView2.layoutManager = layoutManager
                            rv_myRecyclerView2.adapter = adapter

                            adapter!!.notifyDataSetChanged()

                        }else if(document.data["email"] == mAuth!!.currentUser!!.email){
                            var currentNaam = document.data["first"] as String + " " + document.data["last"] as String
                            var currentOrders = document.data["orders"] as ArrayList<HashMap<String, String>>
                            for (i in 0 until currentOrders.size) {
                                var orderDetail = OrderDetails(null, null, null, null, null, null)
                                var long1 = currentOrders[i]["amount1"] as Long
                                var long2 = currentOrders[i]["amount2"] as Long
                                var long3 = currentOrders[i]["amount3"] as Long
                                var long4 = currentOrders[i]["amount3"] as Long

                                orderDetail.adres = currentOrders[i]["adres"]
                                orderDetail.amount1 = long1.toInt()
                                orderDetail.naam = currentNaam
                                orderDetail.amount2 = long2.toInt()
                                orderDetail.amount3 = long3.toInt()
                                orderDetail.amount4 = long4.toInt()
                                allOrders.add(orderDetail)
                            }
                            layoutManager = LinearLayoutManager(this)
                            adapter = OrderListAdapter(allOrders!!, this)
                            rv_myRecyclerView2.layoutManager = layoutManager
                            rv_myRecyclerView2.adapter = adapter

                            adapter!!.notifyDataSetChanged()
                        }

                    }
                }
            }
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_overview -> {
                    startActivity(Intent(this, OverviewActivity::class.java))
                }
                R.id.navigation_bestel -> {
                    startActivity(Intent(this, BestelActivity::class.java))
                }
                R.id.navigation_add -> {
                    startActivity(Intent(this, BestelOverviewActivity::class.java))
                }
            }
            false
        }
}