package com.example.wafelshop

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.wafelshop.domain.Order
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import kotlinx.android.synthetic.main.bestel_koeken.*
import java.util.*

class BestelActivity : AppCompatActivity() {
    var np1var = 0
    var np2var = 0
    var np3var = 0
    var np4var = 0
    lateinit var toolbar: ActionBar

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    fun onComposeAction(mi: MenuItem) {
        val saveAlert = AlertDialog.Builder(this)
        saveAlert.setTitle("Uitloggen")
        saveAlert.setMessage("Zeker dat je wilt uitloggen?")
        saveAlert.setPositiveButton("Ja") { dialogInterface: DialogInterface, i: Int ->
            var mAuth: FirebaseAuth? = FirebaseAuth.getInstance()
            mAuth!!.signOut()
            startActivity(Intent(this, MainActivity::class.java))
        }
        saveAlert.setNegativeButton("Nee") { dialogInterface: DialogInterface, i: Int ->
        }
        saveAlert.show()
    }

    override fun onRestart() {
        super.onRestart()
        startActivity(Intent(this, BestelActivity::class.java));
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        var mAuth: FirebaseAuth? = FirebaseAuth.getInstance()
        if (mAuth!!.currentUser == null) {
            Toast.makeText(applicationContext, "U bent niet ingelogd", Toast.LENGTH_LONG).show()
            startActivity(Intent(this, MainActivity::class.java))
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.bestel_koeken)
        np1.minValue = 0
        np1.maxValue = 30
        np2.minValue = 0
        np2.maxValue = 30
        np3.minValue = 0
        np3.maxValue = 30
        np4.minValue = 0
        np4.maxValue = 30

        toolbar = supportActionBar!!
        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigationView)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        np1.setOnValueChangedListener { picker, oldVal, newVal ->
            np1var = newVal
            total.text = "Total prijs: €${calcTotal()}"
        }
        np2.setOnValueChangedListener { picker, oldVal, newVal ->
            np2var = newVal
            total.text = "Total prijs: €${calcTotal()}"
        }
        np3.setOnValueChangedListener { picker, oldVal, newVal ->
            np3var = newVal
            total.text = "Total prijs: €${calcTotal()}"
        }
        np4.setOnValueChangedListener { picker, oldVal, newVal ->
            np4var = newVal
            total.text = "Total prijs: €${calcTotal()}"
        }
    }



    fun calcTotal(): Double {
        var totalx = 0.0
        totalx = np1var * 1.50 + np2var + np3var + np4var
        return totalx
    }

    fun save(view: View) {
        val saveAlert = AlertDialog.Builder(this)
        saveAlert.setTitle("Bestellen")
        saveAlert.setMessage("Zeker dat je deze bestelling wilt plaatsen?")
        saveAlert.setPositiveButton("Yes") { dialogInterface: DialogInterface, i: Int ->
            Toast.makeText(applicationContext, "Bestelling uitgevoerd", Toast.LENGTH_LONG).show()
            proccesBestelling()
        }
        saveAlert.setNegativeButton("No") { dialogInterface: DialogInterface, i: Int ->
            Toast.makeText(applicationContext, "Bestelling afgebroken", Toast.LENGTH_LONG).show()
        }
        saveAlert.show()
    }

    fun proccesBestelling() {
        val db = FirebaseFirestore.getInstance()

        var mAuth: FirebaseAuth? = FirebaseAuth.getInstance()

        db.collection("users")
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    for (document in task.result!!) {
                        if (document.data["email"] == mAuth!!.currentUser!!.email) {
                            val user = HashMap<String, Any>()
                            var orderList: MutableList<Order> = document.data["orders"] as MutableList<Order>
                            var order = Order(null, null , null,null,null)
                            order.adres = adres.text.toString()
                            order.amount1 = np1var
                            order.amount2 = np2var
                            order.amount3 = np3var
                            order.amount4 = np4var
                            orderList!!.add(order)
                            user["orders"] = orderList
                            db.collection("users").document(document.id)
                                .set(user, SetOptions.merge())
                        }
                    }
                } else {
                    println("Something went horribly wrong")
                    startActivity(Intent(this, MainActivity::class.java))
                }
            }
    }


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_overview -> {
                startActivity(Intent(this, OverviewActivity::class.java))
            }
            R.id.navigation_bestel -> {
                startActivity(Intent(this, BestelActivity::class.java))
            }
            R.id.navigation_add -> {
                startActivity(Intent(this, BestelOverviewActivity::class.java))
            }
        }
        false
    }
}