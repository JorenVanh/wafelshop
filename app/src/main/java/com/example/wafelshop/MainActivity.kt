package com.example.wafelshop

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var email: String? = null
    private var password: String? = null
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialise()
    }

    private fun initialise() {
        mAuth = FirebaseAuth.getInstance()
        btn_login.setOnClickListener {
            if (currentFocus != null) {
                loading(currentFocus)
            } else {
                loginUser()
            }
        }
    }


    private fun loginUser() {
        email = et_email.text.trim().toString()
        password = et_password.text.toString()
        if (!email!!.isEmpty() && !password!!.isEmpty()) {
            mAuth!!.signInWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        val intent = Intent(this, BestelActivity::class.java)
                        startActivity(intent)
                    } else {
                        Toast.makeText(
                            this@MainActivity, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                        startActivity(Intent(this, MainActivity::class.java))
                    }
                }
        } else {
            Toast.makeText(this, "Enter all details", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    fun test(view: View) {
        startActivity(Intent(this, BestelOverviewActivity::class.java))
    }

    fun loading(view: View) {
        var inflater: LayoutInflater? = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?
        var popup: View = inflater!!.inflate(R.layout.loading, null)
        var height: Int = LinearLayout.LayoutParams.WRAP_CONTENT
        var width: Int = LinearLayout.LayoutParams.WRAP_CONTENT
        val popupWindow: PopupWindow = PopupWindow(popup, width, height, false)
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
        loginUser()
    }

    fun signUp(view: View) {
        startActivity(Intent(this, RegisterActivity::class.java))
    }
}
