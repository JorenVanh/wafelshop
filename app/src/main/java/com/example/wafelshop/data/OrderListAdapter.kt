package com.example.wafelshop.data

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.wafelshop.R
import com.example.wafelshop.domain.OrderDetails

class OrderListAdapter(private val list: ArrayList<OrderDetails>, private val context: Context) : RecyclerView.Adapter<OrderListAdapter.ViewHolder>(){
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bindItem(order: OrderDetails){
            var name: TextView = itemView.findViewById(R.id.tvBestelNaam) as TextView
            var adres: TextView = itemView.findViewById(R.id.tvBestelAdres) as TextView
            var amount1: TextView = itemView.findViewById(R.id.tvaantal1) as TextView
            var amount2: TextView = itemView.findViewById(R.id.tvaantal2) as TextView
            var amount3: TextView = itemView.findViewById(R.id.tvaantal3) as TextView
            var amount4: TextView = itemView.findViewById(R.id.tvaantal4) as TextView

            amount1.text = "Aantal koekje 1: " + order.amount1
            amount2.text = "Aantal koekje 2: " + order.amount2
            amount3.text = "Aantal koekje 3: " + order.amount3
            amount4.text = "Aantal koekje 4: " + order.amount4
            name.text = order.naam
            adres.text = order.adres
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): OrderListAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.recyclerview_row2, parent, false)
        return ViewHolder(view)
    }
    override fun getItemCount(): Int {
        return list.size
    }
    override fun onBindViewHolder(holder: OrderListAdapter.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }
}