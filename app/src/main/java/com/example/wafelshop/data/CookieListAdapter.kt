package com.example.wafelshop.data

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.wafelshop.DetailsActivity
import com.example.wafelshop.R
import com.example.wafelshop.domain.Product

class CookieListAdapter (private val list: ArrayList<Product>, private val context: Context): RecyclerView.Adapter<CookieListAdapter.ViewHolder>() {
    inner class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(cookie: Product) {
            var name: TextView = itemView.findViewById(R.id.tvName) as TextView
            var prijs: TextView = itemView.findViewById(R.id.tvPrijs) as TextView
            var pic: ImageView = itemView.findViewById(R.id.imageView) as ImageView
            pic.setImageResource(cookie.pic!!)
            name.text = cookie.kind
            prijs.text = "€"+ cookie.price.toString() + "/koekje"
            itemView.setOnClickListener {

                val intent = Intent(context , DetailsActivity::class.java)
                intent.putExtra("naam", cookie.kind)
                intent.putExtra("prijs" ,"€${cookie.price}/koekje")
                intent.putExtra("pic", cookie.pic!!)
                context.startActivity(intent)
                Toast.makeText(context, cookie.kind, Toast.LENGTH_SHORT).show() }
        }
    }



    override fun onCreateViewHolder(parent: ViewGroup, position: Int): CookieListAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.recyclerview_row, parent, false)
        return ViewHolder(view)
    }
    override fun getItemCount(): Int {return list.size    }
    override fun onBindViewHolder(holder: CookieListAdapter.ViewHolder, position: Int) {        holder.bindItem(list[position])    }
}