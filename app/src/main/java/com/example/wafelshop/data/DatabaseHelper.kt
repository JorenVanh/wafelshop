package com.example.wafelshop.data

import android.app.Person
import android.database.sqlite.SQLiteDatabase
import com.example.wafelshop.domain.Order
import com.example.wafelshop.domain.Product
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils

object DatabaseHelper: OrmLiteSqliteOpenHelper(App.instance, "cookie.db", null, 1) {
    override fun onCreate(database: SQLiteDatabase?, connectionSource: ConnectionSource?) {
        TableUtils.createTableIfNotExists(connectionSource, Product::class.java)
    }

    override fun onUpgrade(
        database: SQLiteDatabase?,
        connectionSource: ConnectionSource?,
        oldVersion: Int,
        newVersion: Int
    ) {
        TableUtils.dropTable<Product, Any>(connectionSource, Product::class.java, true)
        DatabaseHelper.onCreate(database, connectionSource)
    }
}