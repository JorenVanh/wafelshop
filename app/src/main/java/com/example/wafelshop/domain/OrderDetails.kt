package com.example.wafelshop.domain

import java.util.*

class OrderDetails(naam: String? , adres: String?, amount1: Int?, amount2: Int?, amount3: Int?, amount4: Int? ) {
    companion object Factory {
        fun create(naam: String? ,adres: String?, amount1: Int?,amount2: Int?,amount3: Int?, amount4: Int?): OrderDetails {
            return OrderDetails(naam, adres, amount1, amount2, amount3, amount4)
        }
    }
    var naam : String ? = null
    var adres: String ?= null
    var amount1: Int ?= null
    var amount2: Int ?= null
    var amount3: Int ?= null
    var amount4: Int ?= null

}