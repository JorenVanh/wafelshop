package com.example.wafelshop.domain

import java.util.*

class Order(adres: String?, amount1: Int?, amount2: Int?, amount3: Int?, amount4: Int? ) {
    companion object Factory {
        fun create(adres: String?, amount1: Int?,amount2: Int?,amount3: Int?, amount4: Int?): Order {
            return Order(adres, amount1, amount2, amount3, amount4)
        }
    }
    var adres: String ?= null
    var amount1: Int ?= null
    var amount2: Int ?= null
    var amount3: Int ?= null
    var amount4: Int ?= null

}