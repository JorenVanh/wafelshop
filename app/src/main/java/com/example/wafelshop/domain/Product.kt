package com.example.wafelshop.domain

import com.example.wafelshop.data.DatabaseHelper
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable(tableName = "cookie")
data class Product  (
    @DatabaseField(id = true)
    var id: Int ? = 0,
    @DatabaseField
    var kind: String ? = null,
    @DatabaseField
    var price:  Double ? = null,
    @DatabaseField
    var pic: Int ? = null
)

class ProductDao{
    companion object {
        lateinit var dao: Dao<Product, Int>
    }

    init{
        dao = DatabaseHelper.getDao(Product::class.java)
    }

    fun add(product: Product) = dao.createOrUpdate(product)
    fun update(product: Product) = dao.update(product)
    fun delete(product: Product) = dao.delete(product)
    fun queryforAll() = dao.queryForAll()
    fun removerAll(){
        for (table in queryforAll()){
            dao.delete(table)
        }
    }

}

