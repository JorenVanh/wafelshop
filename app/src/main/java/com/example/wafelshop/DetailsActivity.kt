package com.example.wafelshop

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {
    lateinit var toolbar: ActionBar

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    fun onComposeAction(mi: MenuItem) {
        val saveAlert = AlertDialog.Builder(this)
        saveAlert.setTitle("Uitloggen")
        saveAlert.setMessage("Zeker dat je wilt uitloggen?")
        saveAlert.setPositiveButton("Ja") { dialogInterface: DialogInterface, i: Int ->
            var mAuth: FirebaseAuth? = FirebaseAuth.getInstance()
            mAuth!!.signOut()
            startActivity(Intent(this, MainActivity::class.java))
        }
        saveAlert.setNegativeButton("Nee") { dialogInterface: DialogInterface, i: Int ->
        }
        saveAlert.show()
    }

    override fun onRestart() {
        super.onRestart()
        startActivity(Intent(this, OverviewActivity::class.java));
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        var mAuth: FirebaseAuth? = FirebaseAuth.getInstance()
        if (mAuth!!.currentUser == null) {
            Toast.makeText(applicationContext, "U bent niet ingelogd", Toast.LENGTH_LONG).show()
            startActivity(Intent(this, MainActivity::class.java))
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        toolbar = supportActionBar!!
        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigationView)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        imageView.setImageResource(intent.getIntExtra("pic", 1))
        tvName.text = intent.getStringExtra("naam")
        tvPrijs.text = intent.getStringExtra("prijs")
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_overview -> {
                startActivity(Intent(this, OverviewActivity::class.java))
            }
            R.id.navigation_bestel -> {
                startActivity(Intent(this, BestelActivity::class.java))
            }
            R.id.navigation_add -> {
                startActivity(Intent(this, BestelOverviewActivity::class.java))
            }
        }
        false
    }
}
