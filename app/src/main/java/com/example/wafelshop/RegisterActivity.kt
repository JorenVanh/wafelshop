package com.example.wafelshop

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.view.*
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Toast
import com.example.wafelshop.domain.Order
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

import kotlinx.android.synthetic.main.activity_register.*
import java.util.*

class RegisterActivity : AppCompatActivity() {
    var mail: String? = null
    var pass: String? = null

    var firstname: String? = null
    var lastname: String? = null
    var phone: String? = null
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initialise()
    }

    private fun initialise() {
        mAuth = FirebaseAuth.getInstance()
        btn_create.setOnClickListener {
            firstname = et_fname.text.toString()
            lastname = et_lname.text.toString()
            phone = et_phone.text.toString()
            mail = et_email.text.toString()
            pass = et_password.text.toString()
            loading(currentFocus)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    fun onComposeAction(mi: MenuItem) {
        startActivity(Intent(this, MainActivity::class.java))
    }

    fun loading(view: View) {
        var inflater: LayoutInflater? = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?
        var popup: View = inflater!!.inflate(R.layout.loading, null)
        var height: Int = LinearLayout.LayoutParams.WRAP_CONTENT
        var width: Int = LinearLayout.LayoutParams.WRAP_CONTENT
        val popupWindow = PopupWindow(popup, width, height, false)
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
        createNewAccount()
    }

    private fun createNewAccount() {
        if (!pass!!.trim().isEmpty() && !mail!!.trim().isEmpty()) {
            mAuth!!.createUserWithEmailAndPassword(mail!!, pass!!)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        addUserToDatabse()
                        Toast.makeText(this, "User registered", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, MainActivity::class.java))
                    } else {
                        Toast.makeText(this, "Registration failed", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, RegisterActivity::class.java))
                    }
                }
        } else {
            Toast.makeText(this, "Enter all details", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }

    private fun addUserToDatabse() {
        val db = FirebaseFirestore.getInstance()
        val user = HashMap<String, Any>()
        user["first"] = firstname.toString()
        user["last"] = lastname.toString()
        user["email"] = mail.toString()
        user["phone"] = phone.toString()

        var orderList: MutableList<Order> = mutableListOf()
        user["orders"] = orderList

        db.collection("users")
            .add(user)
            .addOnSuccessListener { documentReference ->
                println("succes")
            }
            .addOnFailureListener { e ->
                println("failed")
            }

    }
}
